import java.util.Scanner;

public class Main {
	
	public static void main(String[] args){
		
		EnemyShipFactory shipFactory = new EnemyShipFactory();
		
		EnemyShip enemy = null;
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Select your Ship: (U / B / R)\n");
		
		String userInput = input.nextLine();
		while(userInput != "0"){
			if(input.hasNextLine()){
				String typeOfShip = input.nextLine();
				
				enemy = shipFactory.makeEnemyShip(typeOfShip);
			}
			
			if(enemy != null){
				enemyActions(enemy);
			}
		}
	}
	
	public static void enemyActions(EnemyShip anEnemy){
		
		anEnemy.displatEnemyShip();
		anEnemy.followHeroShip();
		anEnemy.enemyShipShoots();
		
	}
}