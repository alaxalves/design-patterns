public class Jukebox {
	
	SongComponent songList;
	
	public Jukebox(SongComponent newSongList){
		songList = newSongList;
	}
	
	public void getSongList() {
		songList.displaySongInfo();
	}
}