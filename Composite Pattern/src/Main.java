public class Main {
	
	public static void main(String[] args){
		
		SongComponent industrialMusic = new SongGroup("Industrial Music", "is a great kind of music");
		SongComponent hiphopMusic = new SongGroup("HipHop", "was created in the US");
		SongComponent dubstepMusic = new SongGroup("DubStep", "was created in England");
		
		SongComponent allSongs = new SongGroup("Songs List", "Every song there is in the jukebox");
		
		allSongs.add(industrialMusic);
		
		industrialMusic.add(new Song("Dirty Diana", "Michael Jackson", 1987));
		industrialMusic.add(new Song("Bad", "Michael Jackson", 2001));
		industrialMusic.add(new Song("Viena", "Billy Joel", 1994));
		
		industrialMusic.add(dubstepMusic);
		
		dubstepMusic.add(new Song("Love is Dead", "4B & Junkie", 2017));
		dubstepMusic.add(new Song("I Fly With You", "Kasino", 2008));
		dubstepMusic.add(new Song("Rembrandt", "Gigi D'Agostino", 2005));
		
		allSongs.add(hiphopMusic);
		
		hiphopMusic.add(new Song("Go to Church", "Lil Jon", 2009));
		hiphopMusic.add(new Song("Betrayed", "Lil Xan", 2017));
		hiphopMusic.add(new Song("I'm not the only one", "Kevin Gates", 2010));
		
		Jukebox player = new Jukebox(allSongs);
		
		player.getSongList();
		
		
	}
}