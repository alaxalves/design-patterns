public class GrabStocks {
	
	public static void main(String[] args){
		
		StockGrabber stockGrabber = new StockGrabber();
		
		StockObserver obs1 = new StockObserver(stockGrabber);
		
		stockGrabber.setFLKPrice(668.65);
		stockGrabber.setOWLPrice(475.41);
		stockGrabber.setGOOGPrice(874.21);
		
		StockObserver obs2 = new StockObserver(stockGrabber);
		
		stockGrabber.setFLKPrice(567.89);
		stockGrabber.setOWLPrice(347.25);
		stockGrabber.setGOOGPrice(957.17);
		
		stockGrabber.unregister(obs2);
		stockGrabber.unregister(obs1);
		
	}
}