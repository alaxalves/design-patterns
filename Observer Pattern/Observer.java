
public interface Observer {
	
	public void update(double flkPrice, double googPrice, double owlPrice);
	
}
