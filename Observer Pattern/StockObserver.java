public class StockObserver implements Observer {

	private double flkPrice;
	private double owlPrice;
	private double googPrice;
	
	public static int obsIDTracker = 0;
	
	private int observerID;
	
	private Subject stockGrabber;
	
	public StockObserver(Subject stockGrabber){
		
		this.stockGrabber = stockGrabber;
		
		this.observerID = ++obsIDTracker;
		
		System.out.println("New Observer " + this.observerID);
		
		stockGrabber.register(this);
	} 
	
	public void update(double flkPrice, double googPrice, double owlPrice) {
		
		this.flkPrice = flkPrice;
		this.owlPrice = owlPrice;
		this.googPrice = googPrice;
		
		printThePrices();
		
	}
	
	public void printThePrices(){
		
		System.out.println("Observer " + observerID + "\nFALKO: " + flkPrice + 
										"\nOWLA: " + owlPrice + 
										"\nGOOGLE: " + googPrice + "\n");
		
	}
	
}