import java.util.ArrayList;

public class StockGrabber implements Subject {

	private ArrayList<Observer> observers;
	private double flkPrice;
	private double googPrice;
	private double owlPrice;
	
	public StockGrabber(){
		observers = new ArrayList<Observer>();
	}
	
	public void register(Observer newObs) {
		
		 observers.add(newObs);
	}

	public void unregister(Observer removeObs) {
		int observerIndex = observers.indexOf(removeObs);
		
		System.out.println("Observer " + (observerIndex+1) + " deleted");
		
		observers.remove(observerIndex);
	}

	public void notifyObserver() {
		for(Observer obs : observers){
			
			obs.update(flkPrice, googPrice, owlPrice);
		}
	}
	
	public void setFLKPrice(double newFLKPrice){
		this.flkPrice = newFLKPrice;
		notifyObserver();
	}
	
	public void setOWLPrice(double newOWLPrice){
		this.owlPrice = newOWLPrice;
		notifyObserver();
	}
	
	public void setGOOGPrice(double newGOOGPrice){
		this.googPrice = newGOOGPrice;
		notifyObserver();
	}
	
}