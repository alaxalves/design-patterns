public class Main {
	
		public static void main(String[] args){
			EnemyTank tank = new EnemyTank();
			
			EnemyRobot botson = new EnemyRobot();
			
			EnemyAttacker botsonAdapter = new EnemyRobotAdapter(botson);
			
			System.out.println("\nThe Robot");
			
			botson.smashWithHands();
			botson.walkForward();
			botson.reactToHuman("Paul");
			
			System.out.println("\nThe Tank");
			
			tank.fireWeapon();
			tank.driveForward();
			tank.assignDriver("Frank");
			
			System.out.println("\nThe Robot with Adapter");
			
			botsonAdapter.fireWeapon();
			botsonAdapter.driveForward();
			botsonAdapter.assignDriver("Phil");
		}
}