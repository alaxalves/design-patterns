import java.util.Random;

public class EnemyTank implements EnemyAttacker {

	Random gen = new Random();
	
	
	public void fireWeapon() {
		int attackDamage = gen.nextInt(10) + 1;
		
		System.out.println("Enemy Tank does " + attackDamage + " damage");
	}

	public void driveForward() {
		int movement = gen.nextInt(5) + 1;
		
		System.out.println("Enemy Tank moves " + movement + " of distance");		
		
	}

	public void assignDriver(String driverName) {
		
		System.out.println(driverName + " is driving the Tank");	
		
	}
	
}