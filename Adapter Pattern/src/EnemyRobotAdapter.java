//Adapter
public class EnemyRobotAdapter implements EnemyAttacker {
	
	EnemyRobot bot;
	
	public EnemyRobotAdapter(EnemyRobot newBot){
		bot = newBot;
	}
	
	public void fireWeapon() {
		bot.smashWithHands();
	}

	public void driveForward() {
		bot.walkForward();
	}

	public void assignDriver(String driver) {
		bot.reactToHuman(driver); 
	}
	
}