//Adaptee
import java.util.Random;

public class EnemyRobot {
	
	Random gen = new Random();
	
	public void smashWithHands(){
		int attackDamage = gen.nextInt(10) + 1;
		
		System.out.println("Enemy Robot causes " + attackDamage + " damage with hands");
		
	}
	
	public void walkForward() {
		int movement = gen.nextInt(8) + 1;
		
		System.out.println("Enemy Robot walks " + movement + " steps");
	}
	
	public void reactToHuman(String driverName) {
		System.out.println("Enemy Robot didnt accept to be driven by " + driverName);
	}
}